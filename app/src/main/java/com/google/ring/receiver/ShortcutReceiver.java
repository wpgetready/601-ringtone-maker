package com.google.ring.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.ring.utils.AppConstants;

public class ShortcutReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);
        mPrefs.edit().putInt("isCreateShortcut", 1).apply();
        Log.d(AppConstants.log_tag, "onReceive shortcut done");
    }

}