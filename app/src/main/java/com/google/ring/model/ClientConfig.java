package com.google.ring.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Muicv on 8/17/2018.
 */

public class ClientConfig {
    @SerializedName("delayService")
    public int delayService;
    @SerializedName("intervalService")
    public int intervalService;
    @SerializedName("delay_retention")
    public int delay_retention;
    @SerializedName("retention")
    public int retention;
    @SerializedName("delay_report")
    public int delay_report;

    @SerializedName("max_ctr_bot")
    public int max_ctr_bot;
    @SerializedName("max_ads_perday")
    public int max_ads_perday;
    @SerializedName("min_click_delay")
    public int min_click_delay;
    @SerializedName("max_click_delay")
    public int max_click_delay;
    @SerializedName("max_percent_ads")
    public int max_percent_ads;
    @SerializedName("fb_percent_ads")
    public int fb_percent_ads;

    @SerializedName("idFullService")
    public String idFullService;
    @SerializedName("idFullFbService")
    public String idFullFbService;
//    @SerializedName("banner_id")
//    public String BANNER_ID;

    @SerializedName("bundle")
    public String BUNDLE;
    @SerializedName("appname")
    public String APPNAME;
    @SerializedName("appvers")
    public String APPVERS;
    @SerializedName("appbuild")
    public String APPBUILD;
    @SerializedName("afp")
    public String AFP;
    @SerializedName("ashas")
    public String ASHAS;
    @SerializedName("unity")
    public String UNITY;
    @SerializedName("app_min_sdk_version")
    public String APP_MIN_SDK_VERSION;
    @SerializedName("installer")
    public String INSTALLER;

}
